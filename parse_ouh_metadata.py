"""Generate a metadata file for Nextstrain out of sample metadata and Pangolin
and Nextclade results as produced by the pipeline.
"""

__author__ = "Kat Steinke"

import pathlib
import re

from argparse import ArgumentParser
from typing import Dict

import pandas as pd
import yaml

import check_config
import pipeline_config
from helpers import get_number_letter_combination
import version
__version__ = version.__version__



# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

def parse_own_metadata(own_meta_file: pathlib.Path, lineage_file: pathlib.Path,
                       prefix_translation: Dict[str, str]) -> pd.DataFrame:
    """Parse Nextstrain information (where applicable) out of metadata file
    generated during SARS-CoV-2 analysis pipeline.
    Arguments:
        own_meta_file:      File with metadata for sequences for region
        lineage_file:       File with Pangolin and Nextclade results
        prefix_translation: Translation of input sample number prefix to prefix in MADS report

    Returns:
        An overview of the metadata relevant for Nextstrain.
    """
    nextstrain_meta_df = pd.read_csv(own_meta_file, sep="\t",
                                     usecols=["sample_id", "sampling_date"], dtype={"sample_id": str})[["sample_id",
                                                                                                        "sampling_date"]]
    # add Pango lineage data for now
    lineage_df = pd.read_csv(lineage_file, sep=";", usecols=["taxon", "lineage"])
    # drop negative controls early
    lineage_df = lineage_df[~lineage_df["taxon"].str.contains(workflow_config["sample_number_settings"]["negative_control"])]
    # ditto positive controls - these are given as a list
    positive_control_pattern = "|".join(workflow_config["sample_number_settings"]["positive_control"].keys())
    lineage_df = lineage_df[~lineage_df["taxon"].str.contains(positive_control_pattern)]
    # parse sample name out of taxon and change to letter-based
    # as we may have capture groups within the pattern we wrap the whole thing and take the first group
    # TODO: include barcode?
    lineage_df["taxon"] = lineage_df["taxon"].str.extract(f"({workflow_config['sample_number_settings']['sample_number_format']})")[0]
    start_options = "|".join(prefix_translation.keys())
    start_pattern = f"^({start_options})"
    lineage_df["taxon"] = lineage_df["taxon"].apply(lambda x: re.sub(start_pattern,
                                                                                       lambda match:
                                                                                       prefix_translation.get(
                                                                                           match.group(),
                                                                                           match.group()),
                                                                                       x))
    # join dataframes
    nextstrain_meta_df = nextstrain_meta_df.merge(lineage_df, left_on="sample_id", right_on="taxon")
    nextstrain_meta_df["sample_id"] = workflow_config["nextstrain_params"]["sample_prefix"] \
                                      + nextstrain_meta_df["sample_id"]

    nextstrain_meta_df = nextstrain_meta_df.rename(columns={"sample_id": "strain", "sampling_date": "date",
                                                            "lineage": "pango_lineage"})
    nextstrain_meta_df = nextstrain_meta_df.dropna()
    # TODO: add all at once?
    nextstrain_meta_df["virus"] = "ncov"
    nextstrain_meta_df["gisaid_epi_isl"] = ""
    nextstrain_meta_df["genbank_accession"] = ""
    nextstrain_meta_df["region"] = "Europe"
    nextstrain_meta_df["country"] = "Denmark"
    nextstrain_meta_df["division"] = workflow_config["nextstrain_params"]["adm_division"]
    nextstrain_meta_df["location"] = workflow_config["nextstrain_params"]["adm_division"]
    nextstrain_meta_df["region_exposure"] = "Europe"  # TODO: should be able to be changed
    nextstrain_meta_df["country_exposure"] = "Denmark"
    nextstrain_meta_df["division_exposure"] = "Denmark"
    nextstrain_meta_df["segment"] = "genome"
    nextstrain_meta_df["length"] = ""  # TODO: we can get genome length from results if we need
    nextstrain_meta_df["host"] = "Human"
    nextstrain_meta_df["age"] = ""  # TODO: should be able to be changed
    nextstrain_meta_df["sex"] = ""  # TODO: should be able to be changed
    nextstrain_meta_df["originating_lab"] = ""
    nextstrain_meta_df["submitting_lab"] = ""
    nextstrain_meta_df["authors"] = ""
    nextstrain_meta_df["url"] = ""
    nextstrain_meta_df["title"] = ""
    nextstrain_meta_df["date_submitted"] = ""
    # TODO: more elegant rearrangement
    nextstrain_meta_df = nextstrain_meta_df[["strain", "virus", "gisaid_epi_isl", "genbank_accession", "date",
                                             "region", "country", "division", "location", "region_exposure",
                                             "country_exposure", "division_exposure", "segment", "length",
                                             "host", "age", "sex", "originating_lab", "submitting_lab", "authors",
                                             "url", "title", "date_submitted", "pango_lineage"]]
    return nextstrain_meta_df
if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Extract Nextstrain metadata from metadata file generated during SARS-CoV-2"
                                            " analysis pipeline.")
    arg_parser.add_argument("metadata_file", help="Path to metadata file")
    arg_parser.add_argument("result_file", help="Path to Pangolin and Nextclade results")
    arg_parser.add_argument("-o", "--outfile",
                            help="File to output Nextstrain metadata to (default: nextstrain_metadata.tsv)",
                            default="own_nextstrain_metadata.tsv")
    arg_parser.add_argument("--workflow_config_file",
                            help="Config file for run (overrides default config given in script, can be overridden by "
                                 "commandline options)")
    args = arg_parser.parse_args()
    # TODO: more file sanity checks
    own_metafile = pathlib.Path(args.metadata_file)
    lineage_results = pathlib.Path(args.result_file)
    out_file = pathlib.Path(args.outfile)
    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file)
        if not default_config_file.exists():
            raise FileNotFoundError("Config file not found.")
        with open(default_config_file, "r", encoding="utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
        check_config.check_config(workflow_config)
    prefix_translate = get_number_letter_combination(workflow_config["sample_number_settings"]["number_to_letter"],
                                                     workflow_config["sample_number_settings"]["sample_numbers_in"],
                                                     workflow_config["sample_number_settings"]["sample_numbers_out"])
    nextstrain_metafile = parse_own_metadata(own_metafile, lineage_results, prefix_translate)
    nextstrain_metafile.to_csv(path_or_buf=out_file, sep="\t", index=False)
