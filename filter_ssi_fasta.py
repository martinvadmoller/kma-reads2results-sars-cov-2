"""Filter multiFASTA based on samples present in a nextstrain metadata file."""

__author__ = "Kat Steinke"

import pathlib

from argparse import ArgumentParser
from typing import Set

import pandas as pd

from Bio import SeqIO

import version
__version__ = version.__version__


# TODO: redo to take seq record generators?
def extract_named_samples(fasta_in: pathlib.Path,sample_names: Set[str],  fasta_out: pathlib.Path) -> None:
    """Filter a FASTA file so only samples named in a supplied list remain,
    and write the results to a new file.
    Arguments:
        fasta_in:       multiFASTA file from which samples are to be extracted
        sample_names:   Samples to extract
        fasta_out:      multiFASTA file to use as output
    """
    # check that input file exists
    if not fasta_in.exists():
        raise FileNotFoundError("Input FASTA not found.")
    with open(fasta_in, "r", encoding="utf-8") as infile, \
            open(fasta_out, "w", encoding="utf-8") as outfile:
        unfiltered_fastas = SeqIO.parse(infile, "fasta")
        # retain only those sequences whose names we've gotten
        filtered_fastas = (sequence for sequence in unfiltered_fastas if sequence.id in sample_names)
        SeqIO.write(filtered_fastas, outfile, "fasta")

if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Filter a multiFASTA file so only samples found in the date-filtered Nextstrain metadata remain.")
    arg_parser.add_argument("in_fasta", help="Path to multiFASTA file to filter")
    arg_parser.add_argument("metadata", help="Path to Nextstrain metadata file (in tsv format) to filter by")
    arg_parser.add_argument("out_fasta", help="Path to write filtered multiFASTA to")
    args = arg_parser.parse_args()
    in_fasta = pathlib.Path(args.in_fasta)
    meta_file = pathlib.Path(args.metadata)
    out_fasta = pathlib.Path(args.out_fasta)
    if not in_fasta.exists():
        raise FileNotFoundError("Input FASTA not found.")
    if not meta_file.exists():
        raise FileNotFoundError("Metadata file not found")
    # extract strain names from filtered file
    metadata_frame = pd.read_csv(meta_file, sep="\t")
    samples_to_use = set(metadata_frame["strain"])

    extract_named_samples(in_fasta, samples_to_use, out_fasta)
