import pathlib
import unittest

import pytest

from Bio import SeqIO

import filter_ssi_fasta

class TestFilterFasta(unittest.TestCase):
    data_path = pathlib.Path(__file__).parent / "data" / "filter_fasta_test"
    samples_to_use = {"OUH-P21000001", "OUH-P21000002"}
    def test_filter_missing_file(self):
        no_fasta = self.data_path / "no_fasta"
        with pytest.raises(FileNotFoundError, match="Input FASTA not found."):
            filter_ssi_fasta.extract_named_samples(no_fasta, self.samples_to_use, no_fasta)

    def test_filter_success(self):
        in_fasta = self.data_path / "filter_input_fasta.fa"
        out_fasta = self.data_path / "filter_fasta_out.fa"
        target_fasta = self.data_path / "filter_success.fa"
        filter_ssi_fasta.extract_named_samples(in_fasta, self.samples_to_use, out_fasta)
        with open(out_fasta, "r") as testfile:
            test_seqs = list(SeqIO.parse(testfile, "fasta"))
        with open(target_fasta, "r") as target_file:
            target_seqs = list(SeqIO.parse(target_file, "fasta"))
        assert all([test_seq.id == true_seq.id for test_seq, true_seq in zip(sorted(test_seqs,
                                                                                    key=lambda seq: seq.id),
                                                                             sorted(target_seqs,
                                                                                    key=lambda seq: seq.id))])

