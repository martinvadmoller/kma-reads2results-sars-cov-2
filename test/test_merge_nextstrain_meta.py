import pathlib
import unittest

import pandas as pd
import pytest

import merge_ssi_ouh_metadata

class TestMergeNextstrain(unittest.TestCase):
    data_path = pathlib.Path(__file__).parent / "data" / "nextstrain_metadata_test"
    success_frame = pd.DataFrame({"strain": ["OUH-P21000001", "OUH-P21000002"],
                                  "date": ["2021-01-01", "2021-01-02"]})
    success_frame["virus"] = "ncov"
    success_frame["gisaid_epi_isl"] = ""
    success_frame["genbank_accession"] = ""
    success_frame["region"] = "Europe"
    success_frame["country"] = "Denmark"
    success_frame["division"] = "Syddanmark"
    success_frame["location"] = "Syddanmark"
    success_frame["region_exposure"] = "Europe"  # TODO: should be able to be changed
    success_frame["country_exposure"] = "Denmark"
    success_frame["division_exposure"] = "Denmark"
    success_frame["segment"] = "genome"
    success_frame["length"] = ""  # TODO: we can get genome length from results if we need
    success_frame["host"] = "Human"
    success_frame["age"] = ""  # TODO: should be able to be changed
    success_frame["sex"] = ""  # TODO: should be able to be changed
    success_frame["originating_lab"] = ""
    success_frame["submitting_lab"] = ""
    success_frame["authors"] = ""
    success_frame["url"] = ""
    success_frame["title"] = ""
    success_frame["date_submitted"] = ""
    success_frame = success_frame[["strain", "virus", "gisaid_epi_isl", "genbank_accession", "date",
                                   "region", "country", "division", "location", "region_exposure",
                                   "country_exposure", "division_exposure", "segment", "length",
                                   "host", "age", "sex", "originating_lab", "submitting_lab", "authors",
                                   "url", "title", "date_submitted"]]
    def test_success_simple_merge(self):
        test_own_metadata = self.data_path / "test_merge_own_metadata.tsv"
        test_ssi_metadata = self.data_path / "test_merge_ssi_metadata_simple.tsv"

        test_merged_metadata = merge_ssi_ouh_metadata.merge_own_ssi_data(test_own_metadata, test_ssi_metadata)
        test_merged_metadata = test_merged_metadata.fillna("")
        pd.testing.assert_frame_equal(self.success_frame.sort_values(by="strain").reset_index(drop=True),
                                      test_merged_metadata.sort_values(by="strain").reset_index(drop=True))

    def test_success_duplicates(self):
        test_own_metadata = self.data_path / "test_merge_own_metadata.tsv"
        test_ssi_metadata = self.data_path / "test_merge_ssi_metadata_duplicated.tsv"

        test_merged_metadata = merge_ssi_ouh_metadata.merge_own_ssi_data(test_own_metadata, test_ssi_metadata)
        test_merged_metadata = test_merged_metadata.fillna("")
        pd.testing.assert_frame_equal(self.success_frame.sort_values(by="strain").reset_index(drop=True),
                                      test_merged_metadata.sort_values(by="strain").reset_index(drop=True))

    def test_fail_no_metadata(self):
        no_own_metadata = self.data_path / "no_own_metadata"
        test_own_metadata = self.data_path / "test_merge_own_metadata.tsv"
        test_ssi_metadata = self.data_path / "test_merge_ssi_metadata_duplicated.tsv"
        no_ssi_metadata = self.data_path / "no_ssi_metadata"

        with pytest.raises(FileNotFoundError, match="File containing own metadata not found."):
            merge_ssi_ouh_metadata.merge_own_ssi_data(no_own_metadata, test_ssi_metadata)

        with pytest.raises(FileNotFoundError, match="File containing SSI metadata not found."):
            merge_ssi_ouh_metadata.merge_own_ssi_data(test_own_metadata, no_ssi_metadata)


