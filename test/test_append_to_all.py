import pathlib
import unittest

from datetime import date

import pandas as pd
import pytest

import append_to_all_results

class TestMergeDataframes(unittest.TestCase):
    def test_merge_same_columns(self):
        all_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "same_results_base.csv"
        all_data = pd.read_csv(all_results, sep=";",  decimal=",", index_col=False)
        new_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "same_results_new.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        merged_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "same_results_combined.csv"
        merged_data = pd.read_csv(merged_results, sep=";", decimal=",", index_col=False)
        merged_test = append_to_all_results.combine_results(new_data, all_data)
        pd.testing.assert_frame_equal(merged_data, merged_test, check_like=True,
                                      check_datetimelike_compat=True)

    def test_merge_more_to_less(self):
        all_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "old_version_results_base.csv"
        all_data = pd.read_csv(all_results, sep=";", decimal=",", index_col=False)
        new_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "same_results_new.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        merged_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "old_version_results_combined.csv"
        merged_data = pd.read_csv(merged_results, sep=";", decimal=",", index_col=False)
        merged_test = append_to_all_results.combine_results(new_data, all_data)
        print(merged_data["qc.overallScore"])
        print(merged_test["qc.overallScore"])
        pd.testing.assert_frame_equal(merged_data, merged_test, check_like=True,
                                      check_datetimelike_compat=True)

    def test_merge_less_to_more(self):
        all_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "same_results_base.csv"
        all_data = pd.read_csv(all_results, sep=";", decimal=",", index_col=False)
        new_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "old_version_results_new.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        merged_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "less_to_more_results_combined.csv"
        merged_data = pd.read_csv(merged_results, sep=";", decimal=",", index_col=False)
        merged_test = append_to_all_results.combine_results(new_data, all_data)
        pd.testing.assert_frame_equal(merged_data, merged_test, check_like=True,
                                      check_datetimelike_compat=True)

class TestCleanAndMerge(unittest.TestCase):
    # results and MADS data should stay the same
    def setUp(self) -> None:
        merged_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "cleaned_merged.csv"
        self.merged_data = pd.read_csv(merged_results, sep=";", decimal=",", index_col=False,
                                       dtype={"modtaget": str, "analyseret": str,
                                              "sample_number": str})
        # workaround for analysis date (todo: more elegant way?)
        self.merged_data["analyseret"] = date.strftime(date.today(), "%d-%m-%Y")
        self.merged_data = self.merged_data.sort_values(by="taxon", ignore_index=True)
        fake_mads = pathlib.Path(__file__).parent / "data" / "append_to_all" / "fake_mads_data.csv"
        self.mads_data =  pd.read_csv(fake_mads, encoding="latin1", dtype={"afsendt": str,
                                                                            "cprnr.": str,
                                                                            "modtaget": str})
    def test_remove_controls(self):
        new_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "same_results_new.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        clean_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "cleaned_results.csv"
        clean_data = pd.read_csv(clean_results, sep=";", decimal=",", index_col=False,
                                 dtype={"modtaget": str, "analyseret": str,
                                        "sample_number": str})
        clean_data["analyseret"] = date.strftime(date.today(), "%d-%m-%Y")
        samples = pd.DataFrame(data={"KMA nr": ["4099000001", "7099000001", "7099000002",
                                                "7099000003", "NegK2", "18000006", "NegK1"],
                                     "Barkode NB": ["NB18", "NB85", "NB70", "NB11", "NB93",
                                                    "NB95", "NB96"],
                                     "CT/CP værdi": [15, 33, 25.4, 22, 100, 12, 100]})
        test_data = append_to_all_results.clean_and_combine(new_data, clean_data, samples,
                                                            self.mads_data)
        test_data = test_data.sort_values(by="taxon", ignore_index=True)
        pd.testing.assert_frame_equal(test_data, self.merged_data, check_like=True,
                                      check_datetimelike_compat=True, check_dtype=False)


    def test_remove_duplicates(self):
        new_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "same_results_duplicate.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        clean_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "cleaned_results.csv"
        clean_data = pd.read_csv(clean_results, sep=";", decimal=",", index_col=False,
                                 dtype={"modtaget": str, "analyseret": str,
                                        "sample_number": str})
        clean_data["analyseret"] = date.strftime(date.today(), "%d-%m-%Y")
        samples = pd.DataFrame(data={"KMA nr": ["4099000001", "7099000001", "7099000002",
                                                "7099000003", "NegK2", "18000006", "NegK1",
                                                "7099000003"],
                                     "Barkode NB": ["NB18", "NB85", "NB70", "NB11", "NB93",
                                                    "NB95", "NB96", "NB12"],
                                     "CT/CP værdi": [15, 33, 25.4, 22, 100, 12, 100, 30]})
        test_data = append_to_all_results.clean_and_combine(new_data, clean_data, samples,
                                                            self.mads_data)
        test_data = test_data.sort_values(by="taxon", ignore_index=True)
        pd.testing.assert_frame_equal(test_data, self.merged_data, check_like=True,
                                      check_datetimelike_compat=True, check_dtype=False)

    def test_remove_duplicate_of_existing(self):
        new_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "same_results_existing_duplicate.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        clean_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "cleaned_results.csv"
        clean_data = pd.read_csv(clean_results, sep=";", decimal=",", index_col=False,
                                 dtype={"modtaget": str, "analyseret": str,
                                        "sample_number": str})
        clean_data["analyseret"] = date.strftime(date.today(), "%d-%m-%Y")
        samples = pd.DataFrame(data={"KMA nr": ["4099000001", "7099000001", "7099000002",
                                                "7099000003", "NegK2", "18000006", "NegK1",
                                                "4099000002"],
                                     "Barkode NB": ["NB18", "NB85", "NB70", "NB11", "NB93",
                                                    "NB95", "NB96", "NB12"],
                                     "CT/CP værdi": [15, 33, 25.4, 22, 100, 12, 100, 30]})
        test_data = append_to_all_results.clean_and_combine(new_data, clean_data, samples,
                                                            self.mads_data)
        test_data = test_data.sort_values(by="taxon", ignore_index=True)
        pd.testing.assert_frame_equal(test_data, self.merged_data, check_like=True,
                                      check_datetimelike_compat=True, check_dtype=False)

    # test removing research samples
    def test_remove_research(self):
        new_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "same_results_research.csv"
        new_data = pd.read_csv(new_results, sep=";", decimal=",", index_col=False)
        clean_results = pathlib.Path(__file__).parent / "data" / "append_to_all" / "cleaned_results.csv"
        clean_data = pd.read_csv(clean_results, sep=";", decimal=",", index_col=False,
                                 dtype={"modtaget": str, "analyseret": str,
                                        "sample_number": str})
        clean_data["analyseret"] = date.strftime(date.today(), "%d-%m-%Y")
        samples = pd.DataFrame(data={"KMA nr": ["4099000001", "7099000001", "7099000002",
                                                "7099000003", "NegK2", "18000006", "NegK1",
                                                "4099000000"],
                                     "Barkode NB": ["NB18", "NB85", "NB70", "NB11", "NB93",
                                                    "NB95", "NB96", "NB12"],
                                     "CT/CP værdi": [15, 33, 25.4, 22, 100, 12, 100, 30]})
        test_data = append_to_all_results.clean_and_combine(new_data, clean_data, samples,
                                                            self.mads_data)
        test_data = test_data.sort_values(by="taxon", ignore_index=True)
        pd.testing.assert_frame_equal(test_data, self.merged_data, check_like=True,
                                      check_datetimelike_compat=True, check_dtype=False)
