import pathlib
import unittest

from datetime import date, datetime, timedelta

import pandas as pd
import pytest

import parse_ssi_data

class TestParseSSI(unittest.TestCase):
    success_frame = pd.DataFrame({"strain": ["OUH-P21000001", "OUH-P21000002"],
                                  "date": ["2021-01-01", "2021-01-02"],
                                  "pango_lineage": ["B.1.177.21", "B.1.177.21"]})
    success_frame["virus"] = "ncov"
    success_frame["gisaid_epi_isl"] = ""
    success_frame["genbank_accession"] = ""
    success_frame["region"] = "Europe"
    success_frame["country"] = "Denmark"
    success_frame["division"] = "Syddanmark"
    success_frame["location"] = "Syddanmark"
    success_frame["region_exposure"] = "Europe"  # TODO: should be able to be changed
    success_frame["country_exposure"] = "Denmark"
    success_frame["division_exposure"] = "Denmark"
    success_frame["segment"] = "genome"
    success_frame["length"] = ""  # TODO: we can get genome length from results if we need
    success_frame["host"] = "Human"
    success_frame["age"] = ""  # TODO: should be able to be changed
    success_frame["sex"] = ""  # TODO: should be able to be changed
    success_frame["originating_lab"] = ""
    success_frame["submitting_lab"] = ""
    success_frame["authors"] = ""
    success_frame["url"] = ""
    success_frame["title"] = ""
    success_frame["date_submitted"] = ""
    success_frame = success_frame[["strain", "virus", "gisaid_epi_isl", "genbank_accession", "date",
                                   "region", "country", "division", "location", "region_exposure",
                                   "country_exposure", "division_exposure", "segment", "length",
                                   "host", "age", "sex", "originating_lab", "submitting_lab", "authors",
                                   "url", "title", "date_submitted", "pango_lineage"]]
    root_frame = pd.DataFrame(
        [["Wuhan/Hu-1/2019", "ncov", "", "", "2019-12-26", "Asia", "China", "Hubei", "Wuhan", "Asia",
         "China", "Hubei", "genome", "", "Human", "", "", "", "", "", "", "", "", "B"]],
        columns=["strain", "virus", "gisaid_epi_isl", "genbank_accession", "date",
                 "region", "country", "division", "location", "region_exposure",
                 "country_exposure", "division_exposure", "segment", "length",
                 "host", "age", "sex", "originating_lab", "submitting_lab", "authors",
                 "url", "title", "date_submitted", "pango_lineage"],
        )
    success_frame = success_frame.append(root_frame, ignore_index=True)

    def test_fail_date(self):
        input_metadata = pathlib.Path(__file__).parent / "data" / "nextstrain_metadata_test" /  "success_ssi_metadata.tsv"
        with pytest.raises(ValueError, match="Earliest sampling date is in the future."):
            parse_ssi_data.parse_ssi_metadata(input_metadata, sampling_start=date(year=3021,
                                                                                  month=1,
                                                                                  day=1),
                                              sampling_end=date.today())

    def test_parse_success(self):
        input_metadata = pathlib.Path(__file__).parent / "data" / "nextstrain_metadata_test" /  "success_ssi_metadata.tsv"

        test_nextstrain_frame = parse_ssi_data.parse_ssi_metadata(input_metadata, sampling_start=date(year=2021,
                                                                                                      month=1,
                                                                                                      day=1),
                                                                  sampling_end=date.today())
        pd.testing.assert_frame_equal(self.success_frame.sort_values(by="strain").reset_index(drop=True),
                                      test_nextstrain_frame.sort_values(by="strain").reset_index(drop=True))



    def test_later_cutoff(self):
        input_metadata = pathlib.Path(__file__).parent / "data" / "nextstrain_metadata_test" /  "success_ssi_metadata.tsv"

        test_nextstrain_frame = parse_ssi_data.parse_ssi_metadata(input_metadata, sampling_start=date(year=2021,
                                                                                                      month=1,
                                                                                                      day=2),
                                                                  sampling_end=date.today())
        # now we should only get the second sample
        success_frame = self.success_frame[self.success_frame["strain"] != "OUH-P21000001"]

        pd.testing.assert_frame_equal(success_frame.sort_values(by="strain").reset_index(drop=True),
                                      test_nextstrain_frame.sort_values(by="strain").reset_index(drop=True))

    def test_early_cutoff(self):
        input_metadata =pathlib.Path(__file__).parent / "data" / "nextstrain_metadata_test" /  "success_ssi_metadata.tsv"

        test_nextstrain_frame = parse_ssi_data.parse_ssi_metadata(input_metadata, sampling_start=date(year=2020,
                                                                                                      month=12,
                                                                                                      day=1),
                                                                  sampling_end=date(year=2021, month=1, day=1))
        # should only get the first sample now
        success_frame = self.success_frame[self.success_frame["strain"] != "OUH-P21000002"]

        pd.testing.assert_frame_equal(success_frame.sort_values(by="strain").reset_index(drop=True),
                                      test_nextstrain_frame.sort_values(by="strain").reset_index(drop=True))

    def test_hospital_only(self):
        input_metadata = pathlib.Path(
            __file__).parent / "data" / "nextstrain_metadata_test" / "hospital_only_ssi_metadata.tsv"

        test_nextstrain_frame = parse_ssi_data.parse_ssi_metadata(input_metadata, sampling_start=date(year=2021,
                                                                                                      month=1,
                                                                                                      day=1),
                                                                  sampling_end=date.today(), hospital_samples=True)
        print(test_nextstrain_frame)
        print(self.success_frame)
        pd.testing.assert_frame_equal(self.success_frame.sort_values(by="strain").reset_index(drop=True),
                                      test_nextstrain_frame.sort_values(by="strain").reset_index(drop=True))
    def test_malformed_date(self):
        input_metadata = pathlib.Path(__file__).parent / "data" / "nextstrain_metadata_test" /  "malformed_date.tsv"
        with self.assertLogs("extract_metadata") as logged:
            parse_ssi_data.parse_ssi_metadata(input_metadata, sampling_start=date(year=2021, month=1,day=1),
                                              sampling_end=date.today())
            assert "WARNING:extract_metadata:Malformed dates found: 20210101. Samples with malformed dates will be removed." in logged.output
