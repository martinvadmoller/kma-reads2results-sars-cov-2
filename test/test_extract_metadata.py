import pathlib
import re
import unittest

import pandas as pd
import pytest

import extract_metadata


class TestDepthByPosition(unittest.TestCase):
    test_depth_1 = pathlib.Path(__file__).parent / "data" / "test_depths.txt.1.depths"
    test_depth_2 = pathlib.Path(__file__).parent / "data" / "test_depths.txt.2.depths"
    def test_fail_wrong_order(self):
        with pytest.raises(ValueError, match=re.escape("Depth file for read group 1 must end in .1.depths.")):
            extract_metadata.get_highest_depth(self.test_depth_2, self.test_depth_2)
        with pytest.raises(ValueError, match=re.escape("Depth file for read group 2 must end in .2.depths.")):
            extract_metadata.get_highest_depth(self.test_depth_1, self.test_depth_1)

    def test_fail_no_files(self):
        fail_depth_1 = pathlib.Path("no/such/depths/file.txt.1.depths")
        fail_depth_2 = pathlib.Path("no/such/depths/file.txt.2.depths")
        with pytest.raises(FileNotFoundError, match="Depth file for read group 1 not found"):
            extract_metadata.get_highest_depth(fail_depth_1, fail_depth_2)

    def test_fail_no_match(self):
        fail_depth_1 = pathlib.Path("no/such/depths/file.txt.1.depths")
        with pytest.raises(ValueError,
                           match=re.escape("Depth files for different samples supplied. "
                                           "Names of depth files must match until .1/2.depths")):
            extract_metadata.get_highest_depth(fail_depth_1, self.test_depth_2)

    def test_fail_wrong_group(self):
        fail_depth_1 = pathlib.Path(__file__).parent / "data" / "fail_depths.txt.1.depths"
        fail_depth_2 = pathlib.Path(__file__).parent / "data" / "fail_depths.txt.2.depths"
        with pytest.raises(ValueError, match="Depth file for read group 1 contained values for another read group"):
            extract_metadata.get_highest_depth(fail_depth_1, fail_depth_2)


    def test_get_depths_success(self):
        true_depths = pd.DataFrame(data={"position": [1, 2, 3], "max_depth": [400, 200, 350]})
        test_depths = extract_metadata.get_highest_depth(self.test_depth_1, self.test_depth_2)
        pd.testing.assert_frame_equal(true_depths.sort_values(by="position").reset_index(drop=True),
                                      test_depths.sort_values(by="position").reset_index(drop=True))

class TestAverageDepths(unittest.TestCase):
    test_depth_1 = pathlib.Path(__file__).parent / "data" / "test_depths.txt.1.depths"
    test_depth_2 = pathlib.Path(__file__).parent / "data" / "test_depths.txt.2.depths"
    def test_get_depths_success(self):
        max_depths = [400, 200, 350]
        true_average = sum(max_depths) / len(max_depths)
        test_average = extract_metadata.get_average_depth(self.test_depth_1, self.test_depth_2)
        assert true_average == test_average

class TestGetAllDepths(unittest.TestCase):
    def test_fail_wrong_amount_depths(self):
        with pytest.raises(ValueError,
                           match="1 depth files found for sample 18000006_NB01. Exactly 2 depth files are required."):
            extract_metadata.get_all_depths(pathlib.Path(__file__).parent / "data" / "metadata_test"
                                            / "artic_wrong_amount_depths")

    def test_fail_no_depths(self):
        with pytest.raises(FileNotFoundError, match="No depth files found for any sample."):
            extract_metadata.get_all_depths(pathlib.Path(__file__).parent / "data" / "metadata_test"
                                            / "artic_no_depths")

    def test_warn_missing_depths(self):
        with self.assertLogs("extract_metadata") as logged:
            extract_metadata.get_all_depths(pathlib.Path(__file__).parent / "data" / "metadata_test"
                                            / "artic_missing_depths")
            assert "WARNING:extract_metadata:No depth files found for 18000006_NB06" in logged.output

    def test_get_all_depths_success(self):
        all_max_depths = [400, 200, 350]
        all_average = sum(all_max_depths) / len(all_max_depths)
        true_data = pd.DataFrame(data={"sample": ["18000006_NB06", "4021700000_NB04", "4021710000_NB03",
                                                  "7021700000_NB02",
                                                  "7021710000_NB01"],
                                       "coverage": [all_average, all_average, all_average, all_average, all_average]})
        test_data = extract_metadata.get_all_depths(pathlib.Path(__file__).parent / "data" / "metadata_test"
                                                    / "fake_artic")
        pd.testing.assert_frame_equal(true_data, test_data)

class TestGetSequencingProtocol(unittest.TestCase):
    def test_single_protocol(self):
        protocols = {"default": "Artic_v3"}
        protocol_id = "default"
        runsheet = pathlib.Path(__file__).parent / "data" / "metadata_test"\
                   / "Runsheet skabelon-robot.xlsx"
        protocol_found = extract_metadata.get_lab_protocol(runsheet, protocol_id, protocols)
        assert protocol_found == "Artic_v3"

    def test_choose_protocol(self):
        protocols = {"manuel": "Artic_v3", "robot": "Artic_v2"}
        protocol_id = "Kørselstype"
        runsheet = pathlib.Path(__file__).parent / "data" / "metadata_test" \
                   / "Runsheet skabelon-robot.xlsx"
        protocol_found = extract_metadata.get_lab_protocol(runsheet, protocol_id, protocols)
        assert protocol_found == "Artic_v3"


    def test_invalid_choice(self):
        protocols = {"manual": "Artic_v3", "robot": "Artic_v2"}
        protocol_id = "Kørselstype"
        runsheet = pathlib.Path(__file__).parent / "data" / "metadata_test" \
                   / "Runsheet skabelon-robot.xlsx"
        with pytest.raises(KeyError,
                           match="No protocol version found for run type manuel"):
            extract_metadata.get_lab_protocol(runsheet, protocol_id, protocols)

class TestGetMetadata(unittest.TestCase):
    def setUp(self) -> None:
        self.mads_sample_data = pathlib.Path(__file__).parent / "data" / "metadata_test" / "fake_mads_data.csv"
        self.artic_dir_number = pathlib.Path(__file__).parent / "data" / "metadata_test" / "fake_artic"
        self.artic_dir_letter = pathlib.Path(__file__).parent / "data" / "metadata_test" / "fake_artic_letter"
        self.ssi_dir_letter = "210622.4202320"
        self.ssi_dir_number = "210622.4202320_v2"
        self.prefix_mapping = {"70": "P", "40": "H"}
        self.duplicate_numbers = ["P21700000"]
        self.runsheet = pathlib.Path(__file__).parent / "data" / "metadata_test"\
                        / "Runsheet skabelon-robot.xlsx"
    def test_broken_mads(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "metadata_test" / "success_sheet.csv"
        fail_mads = pathlib.Path(__file__).parent / "data" / "no_mads_here.csv"
        with self.assertRaisesRegex(FileNotFoundError, "MADS report file could not be found."):
            extract_metadata.get_metadata(fail_mads, self.artic_dir_letter, test_sheet,
                                          self.runsheet,
                                          self.ssi_dir_letter,
                                          self.prefix_mapping)

    def test_broken_artic(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "metadata_test" /"success_sheet.csv"
        fail_artic = pathlib.Path(__file__).parent / "data" / "not_artic"
        fail_artic_file = pathlib.Path(__file__).parent / "data" / "file_named_artic"
        with self.assertRaisesRegex(FileNotFoundError, "ARTIC directory could not be found."):
            extract_metadata.get_metadata(self.mads_sample_data, fail_artic, test_sheet,
                                           self.runsheet, self.ssi_dir_letter,
                                          self.prefix_mapping)
        with self.assertRaisesRegex(NotADirectoryError,
                                    "Specified path for ARTIC directory is not a directory."):
            extract_metadata.get_metadata(self.mads_sample_data, fail_artic_file, test_sheet,
                                          self.runsheet,
                                          self.ssi_dir_letter,
                                          self.prefix_mapping)

    def test_broken_samplesheet(self):
        no_sheet = pathlib.Path(__file__).parent / "data" / "no_sample_sheet"
        with self.assertRaisesRegex(FileNotFoundError, "Sample sheet could not be found."):
            extract_metadata.get_metadata(self.mads_sample_data, self.artic_dir_letter, no_sheet,
                                          self.runsheet,
                                          self.ssi_dir_letter,
                                          self.prefix_mapping)

    def test_broken_ssi(self):
        blank_ssi = ""
        wrong_ssi = "not.really.ssi"
        test_sheet = pathlib.Path(__file__).parent / "data" / "metadata_test" / "success_sheet.csv"
        with self.assertRaisesRegex(ValueError, "No name given for SSI directory."):
            extract_metadata.get_metadata(self.mads_sample_data, self.artic_dir_letter, test_sheet,
                                          self.runsheet,
                                          blank_ssi,
                                          self.prefix_mapping)
        ssi_not_found = f"SSI directory not found in expected location " \
                        f"({str(self.artic_dir_letter.parent/wrong_ssi)})."
        with pytest.raises(FileNotFoundError, match=re.escape(ssi_not_found)):
            extract_metadata.get_metadata(self.mads_sample_data, self.artic_dir_letter,
                                          test_sheet, self.runsheet, wrong_ssi,
                                          self.prefix_mapping)


    def test_warn_too_many_consensus(self):
        fail_sheet_too_many = pathlib.Path(__file__).parent / "data" / "metadata_test" /"too_many_consensus_sheet.csv"
        with self.assertLogs("extract_metadata") as logged:
            extract_metadata.get_metadata(self.mads_sample_data, self.artic_dir_letter,
                                          fail_sheet_too_many, self.runsheet,
                                          self.ssi_dir_letter, self.prefix_mapping)
            assert "WARNING:extract_metadata:Warning: more consensus files than samples entered" in logged.output
    def test_warn_missing_consensus(self):
        fail_sheet_consensus_missing = pathlib.Path(__file__).parent / "data" / "metadata_test" /"missing_consensus_sheet.csv"

        missing_fasta = ["P87654321_NB25"]
        with self.assertLogs("extract_metadata") as logged:
            extract_metadata.get_metadata(self.mads_sample_data, self.artic_dir_letter,
                                          fail_sheet_consensus_missing, self.runsheet,
                                          self.ssi_dir_letter, self.prefix_mapping)
        warn_missing_fastas = f"Warning: consensus files missing for {str(missing_fasta)}"
        assert f"WARNING:extract_metadata:{warn_missing_fastas}" in logged.output

    def test_warn_duplicate_files(self):
        fail_sheet_duplicates = pathlib.Path(__file__).parent / "data" / "metadata_test" / "duplicate_sheet.csv"
        fail_sheet_input = pathlib.Path(__file__).parent / "data" / "metadata_test" / "duplicate_metadata.csv"
        duplicate_metadata_result = pd.read_csv(fail_sheet_input,
                                                sep=";", dtype={"cpr":str, "kma_id":str})
        duplicate_metadata_result["raw_file_name"] = ""
        duplicate_metadata_result["ct"] = "NA"
        # add coverage
        max_depths = [400, 200, 350]
        true_average = sum(max_depths) / len(max_depths)
        duplicate_metadata_result["coverage"] = true_average
        duplicate_metadata_result["primer_scheme"] = "artic_v4"
        duplicate_metadata_result["pipeline"] = f"Artic_v3_KMAReads2Results_v{extract_metadata.__version__}"
        #duplicate_metadata_result["sample_id"] = duplicate_metadata_result["sample_id"].apply(lambda x: x.replace("P", "70").replace("H", "40"))
        with self.assertLogs("extract_metadata") as logged:
            test_result_duplicate_data = extract_metadata.get_metadata(self.mads_sample_data,
                                                                       self.artic_dir_letter,
                                                                       fail_sheet_duplicates,
                                                                       self.runsheet,
                                                                       self.ssi_dir_letter,
                                                                       self.prefix_mapping)
            warn_duplicate = f"WARNING:extract_metadata:Warning: multiple consensus files for sample(s) " \
                             f"{self.duplicate_numbers}. Select one for manual upload and correct zip archive and " \
                             f"metadata file accordingly."
           # self.assertCountEqual(logged.output,["WARNING:extract_metadata:{}".format(x) for x in [
            #                                                                           warn_duplicate]])
        assert warn_duplicate in logged.output

        print(test_result_duplicate_data)
        pd.testing.assert_frame_equal(test_result_duplicate_data.sort_values(by="sample_id").reset_index(drop=True),
                                      duplicate_metadata_result.sort_values(by="sample_id").reset_index(drop=True),
                                      check_like=False)

    def test_no_cpr_or_date(self):
        fail_sheet_no_cpr = pathlib.Path(__file__).parent / "data" / "metadata_test" /"missing_consensus_sheet.csv"
        with self.assertLogs("extract_metadata") as logged:
            extract_metadata.get_metadata(self.mads_sample_data, self.artic_dir_letter,
                                          fail_sheet_no_cpr, self.runsheet,
                                          self.ssi_dir_letter, self.prefix_mapping)
            missing_fasta = ["P87654321_NB25"]
            warn_no_mads = "Warning: sample(s) ['P87654321'] not found in MADS."
            warn_too_many = "Warning: more consensus files than samples entered"  # TODO: fix missing file?
            warn_missing_fastas = f"Warning: consensus files missing for {str(missing_fasta)}"
            warn_no_cpr = "Warning: no CPR number reported for sample(s) ['P87654321']"
            warn_no_date = "Warning: no sampling date reported for sample(s) ['P87654321']"
            for warning in [f"WARNING:extract_metadata:{x}" for x in [warn_no_mads,
                                                                      warn_missing_fastas,
                                                                      warn_no_cpr,
                                                                      warn_no_date]]:
                assert warning in logged.output


    def test_timestamp(self):
        test_sheet = pathlib.Path(__file__).parent / "data" / "metadata_test" /"success_sheet.csv"
        test_mads = pathlib.Path(__file__).parent / "data" / "fake_mads.csv"
        with self.assertLogs("extract_metadata") as logged:
            extract_metadata.get_metadata(test_mads, self.artic_dir_letter, test_sheet,
                                          self.runsheet, self.ssi_dir_letter,
                                          self.prefix_mapping)
        assert "WARNING:extract_metadata:Warning: MADS report is older than one day." in logged.output
    def test_successful_run(self):
        success_sheet = pathlib.Path(__file__).parent / "data" / "metadata_test" /"success_sheet.csv"
        success_input = pathlib.Path(__file__).parent / "data" / "metadata_test" / "clean_metadata.csv"
        true_success_result = pd.read_csv(success_input, sep=";",
                                          dtype={"cpr":str, "kma_id":str})
        # somewhat inconsistent NA handling needs to be compensated for
        true_success_result["raw_file_name"] = ""
        true_success_result["ct"] = "NA"
        # add coverage
        max_depths = [400, 200, 350]
        true_average = sum(max_depths) / len(max_depths)
        true_success_result["coverage"] = true_average
        true_success_result["primer_scheme"] = "artic_v4"
        true_success_result["pipeline"] = f"Artic_v3_KMAReads2Results_v{extract_metadata.__version__}"
        test_success_sheet = extract_metadata.get_metadata(self.mads_sample_data,
                                                           self.artic_dir_letter, success_sheet,
                                                           self.runsheet,
                                                           self.ssi_dir_letter, self.prefix_mapping)
        print(true_success_result)
        print(test_success_sheet)
        pd.testing.assert_frame_equal(test_success_sheet.sort_values(by="sample_id").reset_index(drop=True),
                                      true_success_result.sort_values(by="sample_id").reset_index(drop=True),
                                      check_like=False)

    def test_translate_numbers(self):
        # sample sheet with 70, 40 and P numbers, plus embedded 70 to ensure it doesn't get changed
        test_translate_sheet = pathlib.Path(__file__).parent / "data" / "metadata_test" / "translate_sheet.csv"
        success_input = pathlib.Path(__file__).parent / "data" / "metadata_test" / "translate_metadata.csv"
        success_result = pd.read_csv(success_input,
                                     sep=";",dtype={"cpr":str, "kma_id":str})
        # somewhat inconsistent NA handling needs to be compensated for
        success_result["raw_file_name"] = ""
        success_result["ct"] = "NA"
        # add coverage
        max_depths = [400, 200, 350]
        true_average = sum(max_depths) / len(max_depths)
        success_result["coverage"] = true_average
        success_result["primer_scheme"] = "artic_v4"
        success_result["pipeline"] = f"Artic_v3_KMAReads2Results_v{extract_metadata.__version__}"
        fake_mads = pathlib.Path(__file__).parent / "data" / "metadata_test" / "fake_mads_data.csv"
        translate_success_sheet = extract_metadata.get_metadata(fake_mads, self.artic_dir_number,
                                                                test_translate_sheet,
                                                                self.runsheet,
                                                                self.ssi_dir_number,
                                                                self.prefix_mapping)
        pd.testing.assert_frame_equal(translate_success_sheet.sort_values(by="sample_id").reset_index(drop=True),
                                      success_result.sort_values(by="sample_id").reset_index(drop=True),
                                      check_like=False)

    def test_handle_numeric_report(self):
        test_translate_sheet = pathlib.Path(__file__).parent / "data" / "metadata_test" / "success_sheet.csv"
        success_input = pathlib.Path(__file__).parent / "data" / "metadata_test" / "translate_metadata_to_number.csv"
        success_result = pd.read_csv(success_input,
                                     sep=";", dtype={"cpr": str, "kma_id": str, "sample_id": str})
        # somewhat inconsistent NA handling needs to be compensated for
        success_result["raw_file_name"] = ""
        success_result["ct"] = "NA"
        # add coverage
        max_depths = [400, 200, 350]
        true_average = sum(max_depths) / len(max_depths)
        success_result["coverage"] = true_average
        success_result["primer_scheme"] = "artic_v4"
        success_result["pipeline"] = f"Artic_v3_KMAReads2Results_v{extract_metadata.__version__}"
        fake_mads = pathlib.Path(__file__).parent / "data" / "metadata_test" / "fake_mads_numeric.csv"
        prefix_mapping = {"P": "70", "H": "40"}
        translate_success_sheet = extract_metadata.get_metadata(fake_mads, self.artic_dir_letter,
                                                                test_translate_sheet, self.runsheet,
                                                                self.ssi_dir_letter, prefix_mapping)

        pd.testing.assert_frame_equal(translate_success_sheet.sort_values(by="sample_id").reset_index(drop=True),
                                      success_result.sort_values(by="sample_id").reset_index(drop=True),
                                      check_like=False)
