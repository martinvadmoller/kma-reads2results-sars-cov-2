"""Filter raw results of running all consensus fastas by samples present in cleaned file."""

__author__ = "Kat Steinke"

import logging
import pathlib
import re
from argparse import ArgumentParser

import pandas as pd
import yaml

import check_config
import pipeline_config
import version

__version__ = version.__version__

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

# start logging
logger = logging.getLogger("filter_results")
logger.setLevel(logging.INFO)
console_log = logging.StreamHandler()
console_log.setLevel(logging.WARNING)
logger.addHandler(console_log)

def filter_by_cleaned(raw_rerun: pd.DataFrame, cleaned_results: pd.DataFrame) -> pd.DataFrame:
    """Filter output of a raw Pangolin/Nextstrain rerun of all consensus files so only curated
    samples (no research samples, no controls, only the best sequence for a given sample) remain.
    Arguments:
         raw_rerun:         Pangolin and Nextclade results after rerunning all sequences with the
                            newest version
         cleaned_results:   The original Pangolin and Nextclade results of each run, filtered to
                            exclude research samples, controls, and duplicates
    Returns:
        The newest Pangolin and Nextclade results for all samples, filtered to exclude research
        samples, controls and duplicates
    """
    # TODO: sanity checks?
    # filter to ensure no duplicates in taxon; keep the best, and make sure NA's are sorted last
    # keep track of how much we had and how much we dropped
    raw_rerun_seqs = len(raw_rerun.index)
    raw_rerun = raw_rerun.sort_values(by=["totalMissing"], na_position="last")
    raw_rerun = raw_rerun.drop_duplicates(subset=["taxon"])
    seqs_no_duplicates = len(raw_rerun.index)
    if raw_rerun_seqs != seqs_no_duplicates:
        logger.info(f"Dropped {raw_rerun_seqs - seqs_no_duplicates} duplicate sequences.")
    raw_rerun = raw_rerun.sort_values(by=["taxon"])
    # inner join so only results present in both are retained - cleaned_results should be the
    # more restrictive one and only contains results with lowest total missing
    # but something might slip in without a new result
    rerun_cleaned = pd.merge(raw_rerun, cleaned_results, how="inner", on="taxon",
                             suffixes=("_rerun", "_original"))
    # identify what has been dropped
    dropped_samples = raw_rerun[~raw_rerun["taxon"].isin(rerun_cleaned["taxon"])]["taxon"].tolist()
    # remove controls since those are uninteresting
    controls = f'({workflow_config["sample_number_settings"]["negative_control"]})|' \
               f'({"|".join(workflow_config["sample_number_settings"]["positive_control"].keys())})'
    dropped_samples = [sample for sample in dropped_samples if not re.search(controls, sample)]
    dropped_samples_pretty = "\n".join(dropped_samples)
    if dropped_samples:
        logger.info(f"Dropped {len(dropped_samples)} samples: \n"
                    f"{dropped_samples_pretty}")
    # only retain the rerun columns
    rerun_columns = [column for column in rerun_cleaned.columns.values
                     if column.endswith("_rerun")]
    # there is only one taxon column - prepend this
    rerun_columns.insert(0, "taxon")
    # append metadata
    sample_metadata = ['modtaget', 'CT/CP værdi', 'sample_number', 'analyseret']
    rerun_columns.extend(sample_metadata)
    rerun_cleaned = rerun_cleaned[rerun_columns]
    # clean up names
    rerun_cleaned = rerun_cleaned.rename(lambda x: x.replace("_rerun", ""), axis="columns")
    return rerun_cleaned

if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Filter output of a raw Pangolin/Nextstrain rerun of "
                                            "all consensus files so only curated samples "
                                            "(no research samples, no controls, only the best "
                                            "sequence for a given sample) remain.")
    arg_parser.add_argument("rerun_results", help="Path to unfiltered results of reanalysis")
    arg_parser.add_argument("filtered_results", help="Path to pre-filtered original results")
    arg_parser.add_argument("--outfile", "-o", help="Path to save filtered reanalysis results to",
                            default="filtered_rerun.csv")
    arg_parser.add_argument('-l', '--logfile',
                            help = "Path to logfile for warnings (default: metadata_log.txt)",
                            default = "filter_data_log.txt")
    arg_parser.add_argument("--workflow_config_file",
                            help = "Config file for run (overrides default config given in script,"
                                   " can be overridden by commandline options)")

    args = arg_parser.parse_args()
    rerun_results = pathlib.Path(args.rerun_results)
    filtered_results = pathlib.Path(args.filtered_results)
    outfile = pathlib.Path(args.outfile)

    logfile_path = pathlib.Path(args.logfile)
    # log to file
    log_file = logging.FileHandler(logfile_path)
    log_file.setLevel(logging.INFO)
    logfile_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    log_file.setFormatter(logfile_formatter)
    logger.addHandler(log_file)

    if args.workflow_config_file:
        default_config_file = pathlib.Path(args.workflow_config_file)
        if not default_config_file.exists():
            raise FileNotFoundError("Config file not found.")
        with open(default_config_file, "r", encoding = "utf-8") as config_file:
            workflow_config = yaml.safe_load(config_file)
        check_config.check_config(workflow_config)

    if not rerun_results.exists():
        raise FileNotFoundError("Results of reanalysis not found.")
    if not filtered_results.exists():
        raise FileNotFoundError("Cleaned results not found.")
    rerun_data = pd.read_csv(rerun_results, sep=";", decimal=",", index_col=False)
    filtered_data = pd.read_csv(filtered_results, sep=";", decimal=",", index_col=False)
    cleaned_rerun = filter_by_cleaned(rerun_data, filtered_data)
    cleaned_rerun.to_csv(path_or_buf=outfile, sep=";", decimal=",", index=False)