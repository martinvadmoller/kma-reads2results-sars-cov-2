__author__ = "Kat Steinke"

import logging
import pathlib
import re
import sys
import warnings

import pandas as pd
import yaml

import check_config
import version
__version__ = version.__version__

# this will only run with default config anyway
default_config_file = (pathlib.Path(__file__).parent / "config" / "pipeline_routine.yaml").resolve()
with open(default_config_file, "r") as default_config:
    workflow_config = yaml.safe_load(default_config)
check_config.check_config(workflow_config)

# convert sample numbers to MADS-friendly format
# see if any weren't found, point at relevant number in runsheet if so

# TODO: refactor to fix code reuse

# TODO: should this raise exceptions or just output an overview?

logger = logging.getLogger("check_runsheet")
logger.setLevel(logging.INFO)
console_log = logging.StreamHandler()
console_log.setLevel(logging.WARNING)
logger.addHandler(console_log)

def check_by_prefix(sheet_data: pd.DataFrame, lab_data: pd.DataFrame, sheet_prefix: str, lab_data_prefix: str) \
    -> None:
    """Check whether samples of a given sample type (indicated by prefix) are contained in a report from the
    laboratory information system.
    Arguments:
        sheet_data:         Data contained in runsheet
        lab_data:           Data contained in laboratory information system report (minimum: sample numbers and date
                            received)
        sheet_prefix:       Sample number prefix designating desired sample type in sample sheet
        lab_data_prefix:    Sample number prefix corresponding to sheet_prefix in the format used in the LIS report
    """
    runsheet_filtered = sheet_data[sheet_data["KMA nr"].str.startswith(sheet_prefix)].copy()
    # check if this leaves us with any data
    if runsheet_filtered.empty:
        raise ValueError(f"No samples with prefix {sheet_prefix} found in runsheet.")
    # piece sample number together: get the prefix, identify the year, then add the last six characters
    # prefix is already known
    runsheet_filtered["proevenr_prefix"] = sheet_prefix
    # extract last six characters
    runsheet_filtered["proevenr_kort"] = runsheet_filtered["KMA nr"].str.slice(start=-6)
    # to find year, check lab information system data and go for date *received*
    # extract relevant samples again
    lab_data_filtered = lab_data[lab_data["prøvenr"].str.startswith(lab_data_prefix)].copy()
    # get last six characters of sample number to match the one from the runsheet
    lab_data_filtered["proevenr_kort"] = lab_data_filtered["prøvenr"].str.slice(start=-6)
    # check if there are mismatches between runsheet and MADS data
    missing_from_mads = runsheet_filtered[~runsheet_filtered["proevenr_kort"].isin(lab_data_filtered["proevenr_kort"])][
        "KMA nr"].dropna().tolist()
    if missing_from_mads:
        raise ValueError(
            f"Samples {missing_from_mads} were not found in MADS report. Please check that sample numbers are correct.")


def check_runsheet(sheet_data: pd.DataFrame, lab_report: pathlib.Path) -> None:
    """Check if sample numbers are found in laboratory information system report.
    Arguments:
        sheet_data:   DataFrame representation of samples in runsheet
        lab_report: Path to lab information system's report to check for presence of samples
    """
    # needs prefix parsing, so set this up in the beginning
    prefix_mapping = workflow_config["sample_number_settings"]["number_to_letter"]
    # load and prepare LIS report

    lab_info_data = pd.read_csv(lab_report, encoding="latin1", dtype={"afsendt": str, "cprnr.": str,
                                                                           "modtaget": str})
    # sample numbers can be identical except for the prefix -> make subsets of sample sheet and report by prefix
    # remove both negative and positive controls here
    all_prefixes = sheet_data["KMA nr"].apply(lambda x: x[:2]
                                              if not (re.match(workflow_config["sample_number_settings"]["negative_control"], x)
                                                      or x in workflow_config["sample_number_settings"]["positive_control"])
                                              else pd.NA)
    unique_prefixes = all_prefixes.dropna().unique()
    # record errors if they happen here
    errors = []
    for prefix in unique_prefixes:
        try:
            check_by_prefix(sheet_data, lab_info_data, prefix, prefix_mapping[prefix])
        except ValueError as value_err:
            errors.append(value_err)

    if errors:
        print("The following issues were encountered:")
        error_text ="\n".join([str(parsing_error) for parsing_error in errors])
        raise ValueError(error_text)
    else:
        print("The runsheet is correct.")

def check_sample_numbers(sheet_data: pd.DataFrame) -> None:
    """Check if sample numbers are present and in the correct format.
    Arguments:
        sheet_data:   DataFrame representation of samples in runsheet
    """
    sheet_data = sheet_data.copy()
    no_sample_ids = sheet_data["KMA nr"].isna().all()
    if no_sample_ids:
        raise ValueError("No sample IDs found.")
    # TODO: just drop NA?
    # check duplicates early - this only needs to warn, not break
    if any(sheet_data["KMA nr"].dropna().duplicated()):
        duplicated_ids = sheet_data["KMA nr"][sheet_data["KMA nr"].duplicated()].dropna().unique()
        logger.warning(f"Sample number(s) {duplicated_ids} are duplicated. If you are sure you want to sequence the same sample twice, you can ignore this warning.")
    # check that positive and negative controls are included
    # for positive controls: see if there are any sample numbers matching the controls
    positive_controls_in_sheet = set(workflow_config["sample_number_settings"]["positive_control"]).intersection(sheet_data["KMA nr"])
    if not positive_controls_in_sheet:
        raise ValueError(f"No positive controls given in runsheet.")
    # for negative controls: see if there is anything matching negative control pattern
    negative_controls_in_sheet = sheet_data["KMA nr"].str.fullmatch(workflow_config["sample_number_settings"]["negative_control"],
                                                                    na = False)
    if not negative_controls_in_sheet.any():
        raise ValueError("No negative controls given in runsheet.")
    # positive control format has been checked, remove positive controls before checking the rest
    # TODO: could be more explicit by compiling positive control list as pattern but likely at the cost of speed
    sheet_data = sheet_data.loc[~sheet_data["KMA nr"].isin(workflow_config["sample_number_settings"]["positive_control"])]
    id_pattern = '^(' \
                 + workflow_config["sample_number_settings"]["sample_number_format"] \
                 + '|' \
                 + workflow_config["sample_number_settings"]["negative_control"] \
                 + ')$'
    fail_ids = sheet_data["KMA nr"][~sheet_data["KMA nr"].apply(str).str.match(id_pattern, na=False)].dropna().tolist()
    if workflow_config['sample_number_settings']['sample_numbers_in'] == "number":
        allowed_start = workflow_config['sample_number_settings']['number_to_letter'].keys()
    else:
        allowed_start = workflow_config['sample_number_settings']['number_to_letter'].values()
    # TODO: make sample number length variable?
    if fail_ids:
        raise ValueError(f"""\nSample IDs {fail_ids} are not valid. \
Sample IDs must start with {" or ".join(allowed_start)} followed by eight numbers (six if leaving out year). \
Negative controls must be given in the format {workflow_config['sample_number_settings']['negative_control']}. \
Please correct sample IDs in runsheet.""")


if __name__ == "__main__":
    # suppress openpyxl warning - not relevant for data processing
    warnings.filterwarnings('ignore', message="Data Validation extension is not supported and will be removed",
                            module="openpyxl")
    print("###Runsheet check")
    run_sheet = pathlib.Path(input("Enter path to runsheet: ").strip().strip("'")).resolve()
    print("Loading runsheet...\n")
    runsheet_data = pd.read_excel(run_sheet, usecols="A", skiprows=3, # don't check CP for now
                               dtype={"KMA nr": str})
    runsheet_data = runsheet_data.dropna()
    print("Checking runsheet format....\n")
    # simple error handling, suppressing tracebacks
    try:
        check_sample_numbers(runsheet_data)
    except ValueError as value_error:
        logger.error(str(value_error))
        sys.exit()
    lab_info_report = pathlib.Path(workflow_config["lab_info_system"]["lis_report"])
    print("Comparing to samples in MADS......\n")
    try:
        check_runsheet(runsheet_data, lab_info_report)
    except ValueError as value_error:
        logger.error(str(value_error))
        sys.exit()
