"""
Automatically generate a report screening for rows containing one defined item in one defined column
of the combined Pangolin/Nextclade report. Helper for any trivial but repeated screening tasks.
"""
__author__ = "Kat Steinke"
__version__ = "1.0.1+local"

import pathlib

from argparse import ArgumentParser

import pandas as pd

def screen_for_flag(report: pd.DataFrame, column_to_screen: str, search_term: str) -> pd.DataFrame:
    """Filter a dataframe down to only rows containing a given search term in a given column.
    Arguments:
        report:             combined Pangolin and Nextclade results
        column_to_screen:   column in which to search for desired term
        search_term:        term by which to filter dataframe
    Returns:
        A DataFrame only containing rows that match the search frame (blank if empty).
    """
    if column_to_screen not in report.columns:
        raise KeyError(f"Column {column_to_screen} not found in report.")
    screened_report = report[report[column_to_screen].str.contains(search_term, na=False)]
    return screened_report

if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Filter pangolin/nextclade results to only rows containing a given search"
                                            "term in a given column")
    arg_parser.add_argument("results", help="Path to file with pangolin and nextclade results")
    arg_parser.add_argument("--search_column", help="Column to search for search term", default="scorpio_call")
    arg_parser.add_argument("--search_term", help="Term by which to filter dataframe", default="Probable")
    arg_parser.add_argument("-o", "--outfile", help="Path to write output file to", default="filtered_results.csv")
    args = arg_parser.parse_args()
    combined_results = pathlib.Path(args.results)
    if not combined_results.exists():
        raise FileNotFoundError("File with pangolin and nextclade results not found.")
    search_column = args.search_column
    searchterm = args.search_term
    outfile = pathlib.Path(args.outfile)

    combined_report = pd.read_csv(combined_results, sep=";")

    screened_results = screen_for_flag(combined_report, search_column, searchterm)
    screened_results.to_csv(outfile, sep=";", index=False)
