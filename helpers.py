"""Assorted helper functions for the pipeline."""

__author__ = "Kat Steinke"

import re
from typing import Dict

import pipeline_config
import version
__version__ = version.__version__

# import parameters
default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

def map_sample_number_to_letter(sample_hit: re.Match) -> str:
    """Replace numbers in beginning of sample number with corresponding letters."""
    value_found = sample_hit.group(0)
    if value_found in workflow_config["sample_number_settings"]["number_to_letter"]:
        mapped = workflow_config["sample_number_settings"]["number_to_letter"][value_found]
    else:
        mapped = value_found
    return mapped

def map_sample_letter_to_number(sample_hit: re.Match) -> str:
    """Replace letters in beginning of sample number with corresponding numbers."""
    # start by reversing number_to_letter
    letter_to_number = {letter: number
                        for (number, letter) in workflow_config["sample_number_settings"]["number_to_letter"].items()}
    value_found = sample_hit.group(0)
    if value_found in letter_to_number:
        mapped = letter_to_number[value_found]
    else:
        mapped = value_found
    return mapped

def double_up_escapes(pattern: str) -> str:
    """Duplicate curly braces and backslashes to make a regex pattern usable in a snakemake shell command."""
    pattern = pattern.replace("{", "{{").replace("}", "}}").replace("\\", "\\\\")
    return pattern

def get_number_letter_combination(number_to_letter: Dict[str, str], samples_in: str,
                                        samples_out: str) -> Dict[str, str]:
    """Generate correct mapping for a sample number setup from a number to letter mapping.
    Arguments:
        number_to_letter:   The original number to letter mapping
        samples_in:         The format sample numbers have in the runsheet
        samples_out:        The format sample numbers have in the laboratory information system
    Returns:
        A mapping of sample number starts in samples_in format to sample numbers in samples_out format.
    """
    if samples_in  not in {"number", "letter"}:
        raise ValueError(f"Invalid initial sample format {samples_in}. Sample format can only be number or letter")
    if samples_out not in {"number", "letter"}:
        raise ValueError(f"Invalid desired sample format {samples_out}. Sample format can only be number or letter")
    if samples_in == "number":
        if samples_out == "letter":
            return number_to_letter
        # otherwise, samples_out must be number, so we return a self-to-self mapping
        return {key: key for key in number_to_letter}
    else: # samples_in is letter
        if samples_out == "number":
            # reverse the dictionary
            return {value:key for (key,value) in number_to_letter.items()}
        # otherwise, samples_out is letter
        return {value:value for value in number_to_letter.values()}
