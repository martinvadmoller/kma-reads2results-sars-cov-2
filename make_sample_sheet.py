"""
Create sample sheet from runsheet (workaround to avoid run directive in Snakemake).
"""


__author__ = "Kat Steinke"

import pathlib
import re
import warnings

from argparse import ArgumentParser
from datetime import datetime
from typing import Dict, List, Optional

import pandas as pd
import yaml

import check_config
import pipeline_config
from helpers import get_number_letter_combination
import version
__version__ = version.__version__

# suppress openpyxl warning about DataValidation as it doesn't impact data
warnings.filterwarnings('ignore',
                        message="Data Validation extension is not supported and will be removed",
                        module="openpyxl")

def fix_ids_by_prefix(runsheet_data: pd.DataFrame, lab_data: pd.DataFrame, sheet_prefix: str,
                      lab_data_prefix: str) -> pd.DataFrame:
    """Fix IDs in runsheet (by identifying and adding year) for a given sample type
    (indicated by sample number prefixes).
    Arguments:
        runsheet_data:      Data contained in runsheet
        lab_data:           Data contained in laboratory information system report
                            (minimum: sample numbers and date received)
        sheet_prefix:       Sample number prefix designating desired sample type in sample sheet
        lab_data_prefix:    Sample number prefix corresponding to sheet_prefix in the format
                            used in the LIS report
    Returns:
        A dataframe with sample ID, barcodes and Ct value from the runsheet, with the year
        identified from the LIS report and added to the sample ID correspondingly.
    """
    # reduce runsheet to only the relevant samples - make a copy to ensure the original stays as is
    runsheet_filtered = runsheet_data[runsheet_data["KMA nr"].str.startswith(sheet_prefix)].copy()
    # check if this leaves us with any data
    if runsheet_filtered.empty:
        raise ValueError(f"No samples with prefix {sheet_prefix} found in runsheet.")
    # piece sample number together: get the prefix, identify the year, add the last six characters
    # prefix is already known
    runsheet_filtered["proevenr_prefix"] = sheet_prefix
    # extract last six characters
    runsheet_filtered["proevenr_kort"] = runsheet_filtered["KMA nr"].str.slice(start=-6)

    # to find year, check lab information system data and go for date *received*
    # extract relevant samples again
    lab_data["prøvenr"] = lab_data["prøvenr"].astype(str)
    lab_data_filtered = lab_data[lab_data["prøvenr"].str.startswith(lab_data_prefix)].copy()
    # get last six characters of sample number to match the one from the runsheet
    lab_data_filtered["proevenr_kort"] = lab_data_filtered["prøvenr"].str.slice(start=-6)

    # check if there are duplicates in this - should not happen
    if any(lab_data_filtered.duplicated(subset=["proevenr_kort"])):
        raise ValueError("MADS report contains duplicated sample numbers. "
                         "This likely means the report covers multiple years. "
                         "Get a new MADS report with the correct start date.")
    # check if there are mismatches between runsheet and MADS data
    missing_from_mads = runsheet_filtered[~runsheet_filtered["proevenr_kort"].isin(lab_data_filtered["proevenr_kort"])]["KMA nr"].dropna().tolist()
    if missing_from_mads:
        raise ValueError(f"Samples {missing_from_mads} were not found in MADS report. "
                         f"Please check that sample numbers are correct.")
    # join subsets of sheet and report - don't drop any samples not in the report!
    runsheet_filtered = runsheet_filtered.merge(lab_data_filtered, on="proevenr_kort", how="left")
    runsheet_filtered["modtaget"] = runsheet_filtered["modtaget"].apply(lambda x:
                                                                        datetime.strptime(str(x),
                                                                                          "%d%m%Y").date()
                                                                        if pd.notna(x)
                                                                        else x)
    runsheet_filtered["år_modtaget"] = runsheet_filtered["modtaget"].apply(lambda x: x.strftime("%y")
                                                                           if pd.notna(x)
                                                                           else "")
    runsheet_filtered["KMA nr"] = runsheet_filtered["proevenr_prefix"] \
                                  + runsheet_filtered["år_modtaget"]\
                                  + runsheet_filtered["proevenr_kort"]
    return runsheet_filtered[["KMA nr", "Barkode NB", "CT/CP værdi"]]


# TODO: fallback for if no LIS report?
def parse_and_fix_ids(run_sheet: pathlib.Path, lab_info_report: pathlib.Path,
                      prefix_translation: Optional[Dict[str, str]] = None,
                      negk_format: Optional[str]='NegK[0-9]*',
                      positive_controls: Optional[List[str]]=None) -> pd.DataFrame:
    """Parse a runsheet, extract sample ID, barcodes and Ct value, and add year to sample ID
    if needed.
    Arguments:
        run_sheet:          Path to runsheet
        lab_info_report:    Path to report from laboratory information system containing
                            sample numbers and date received
        prefix_translation: Mapping of numbers to letters in sample number
        negk_format:        Format of negative control
        positive_controls:  Sample numbers for positive controls
    Returns:
        A dataframe containing sample ID in number format, barcodes and Ct value
    """
    # load and prepare runsheet and LIS report
    sheet_data = pd.read_excel(run_sheet, usecols="A:C", skiprows=3, dtype={"KMA nr": str,
                                                                            "Barkode NB": str})
    sheet_data = sheet_data.dropna()

    lab_info_data = pd.read_csv(lab_info_report, encoding="latin1", dtype={"afsendt": str,
                                                                           "cprnr.": str,
                                                                           "modtaget": str})

    # for each prefix, extract relevant data and join on these

    errors = []
    sheets = []

    if not prefix_translation:
        prefix_translation = {"70": "P", "40": "H"}

    if not positive_controls:
        positive_controls = ['34567890', '45678901'] # TODO: currently dummy numbers
    # get prefixes for all except the negative controls
    prefix_length = len(list(prefix_translation.keys())[0]) # TODO: make pretty
    all_prefixes = sheet_data["KMA nr"].apply(lambda x: x[:prefix_length]
                                                if not (re.match(negk_format, x)
                                                        or x in positive_controls)
                                               else pd.NA)
    unique_prefixes = all_prefixes.dropna().unique()
    for prefix in unique_prefixes:
        try:
            sheet_data_for_prefix = fix_ids_by_prefix(sheet_data, lab_info_data, prefix,
                                                      prefix_translation[prefix])
            sheets.append(sheet_data_for_prefix)
        except ValueError as value_err:
            errors.append(value_err)

    if errors:
        print("The following issues were encountered:")
        error_text ="\n".join([str(parsing_error) for parsing_error in errors])
        raise ValueError(error_text)
    # readd controls
    sheet_data_negk = sheet_data[sheet_data["KMA nr"].str.match(negk_format)]
    sheet_data_posk = sheet_data[sheet_data["KMA nr"].isin(positive_controls)]
    sheets.extend([sheet_data_negk, sheet_data_posk])
    sheet_data = pd.concat(sheets)
    # TODO: warn for missing data?
    return sheet_data


if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Extract sample ID and barcode from a runsheet "
                                            "and write to sample sheet")
    arg_parser.add_argument("runsheet", help="Path to run sheet")
    arg_parser.add_argument("report_file", help="Path to laboratory information system report")
    arg_parser.add_argument("outfile", help="Desired output file")
    arg_parser.add_argument("--config_file",
                            help=f"Config file for run "
                                 f"(default: {pipeline_config.default_config_file})",
                            default=pipeline_config.default_config_file)
    args = arg_parser.parse_args()
    runsheet = pathlib.Path(args.runsheet)
    report_file = pathlib.Path(args.report_file)
    outfile = pathlib.Path(args.outfile)
    configfile = pathlib.Path(args.config_file)
    if not configfile.exists():
        raise FileNotFoundError("Config file not found.")
    with open(configfile, "r", encoding="utf-8") as config_file:
        workflow_config = yaml.safe_load(config_file)
    check_config.check_config(workflow_config)
    # check in which format samples should be returned
    samples_translated = get_number_letter_combination(workflow_config["sample_number_settings"]["number_to_letter"],
                                                       workflow_config["sample_number_settings"]["sample_numbers_in"],
                                                       workflow_config["sample_number_settings"]["sample_numbers_out"])
    negative_control = workflow_config["sample_number_settings"]["negative_control"]
    positive_control = list(workflow_config["sample_number_settings"]["positive_control"].keys())
    samplesheet = parse_and_fix_ids(runsheet, report_file, samples_translated, negative_control,
                                    positive_control)
    samplesheet.to_csv(path_or_buf=outfile, index=False)
