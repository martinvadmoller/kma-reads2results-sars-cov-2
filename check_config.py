"""Check that settings specified in config file are valid."""

__author__ = "Kat Steinke"

from typing import Dict

import version
__version__ = version.__version__

def check_config(workflow_config: Dict) -> None:
    """Check config file for invalid settings and raise an error for all at once.
    Arguments:
        workflow_config:    Parsed configuration taken from workflow's config file
    """
    # collect errors and return everything at once
    error_message = "The following issue(s) were detected with the config file:"
    fail_record = []
    if not workflow_config["sample_number_settings"]["sample_numbers_in"] in {"number", "letter"}:
        fail_record.append("Format of sample numbers used as input must be given as 'number' or 'letter'.")
    if not workflow_config["sample_number_settings"]["sample_numbers_out"] in {"number", "letter"}:
        fail_record.append("Desired format of sample numbers in output must be given as 'number' or 'letter'.")
    if not workflow_config["primer_version"] in {"3", "4", "4.1", "1200", "1200.3"}:
        fail_record.append(f"Invalid primer scheme {workflow_config['primer_version']}. "
                           f"Primer scheme must be 3, 4, 4.1, 1200, or 1200.3.")
    if not workflow_config['conda_frontend'] in {"conda", "mamba"}:
        fail_record.append(f"Invalid conda frontend {workflow_config['conda_frontend']}. "
                           f"Conda frontend must be conda or mamba.")
    if not workflow_config["artic"]["workflow"] in {"nanopolish", "medaka"}:
        fail_record.append(f"Invalid ARTIC workflow {workflow_config['artic']['workflow']}."
                           f" Workflow options are nanopolish or medaka.")
    if not workflow_config["pango_prediction"] in {"pangolearn", "usher"}:
        fail_record.append(
            f"Invalid Pangolin prediction setting {workflow_config['pango_prediction']}. "
            f"Prediction settings are pangolearn or usher.")
    if fail_record:
        raise ValueError(error_message + "\n" + "\n".join(fail_record))
