"""Append current results to raw and cleaned versions of all_results."""

__author__ = "Kat Steinke"

import pathlib
import re
from argparse import ArgumentParser
from datetime import date, datetime

import pandas as pd
import yaml

import check_config
import pipeline_config
import version
from helpers import get_number_letter_combination

__version__ = version.__version__

default_config_file = pipeline_config.default_config_file
workflow_config = pipeline_config.WORKFLOW_DEFAULT_CONF

def combine_results(new_results: pd.DataFrame, existing_results: pd.DataFrame) -> pd.DataFrame:
    """Append new combined Pangolin/Nextclade results to existing data,
    making sure that errors always are listed last.
    Arguments:
        new_results:        Results to add to existing data
        existing_results:   Overview (semicolon-separated CSV) of results to add to
    Returns:
        A DataFrame representation of the combined results.
    """
    merged_results = pd.concat([existing_results, new_results], ignore_index=True)
    # move errors to the end
    results_colnames = list(merged_results.columns.values)
    results_colnames.remove("errors")
    results_colnames.append("errors")
    return merged_results[results_colnames]


def clean_and_combine(new_results: pd.DataFrame, clean_results: pd.DataFrame,
                      samples: pd.DataFrame, lis_report: pd.DataFrame) -> pd.DataFrame:
    """Clean combined Pangolin/Nextclade results by removing controls and research samples,
    add metadata from lab information system and runsheet and append to cleaned results.
    Arguments:
        new_results:    Results to append to the "cleaned" results
        clean_results:  Overview (semicolon-separated CSV) of results to add to, cleaned to
                        remove duplicates, positive and negative controls as well as
                        research samples (as marked by CPR consisting of zeroes) and extended with
                        dates for when a sample was received and analyzed, as well as Ct values
        samples:        Overview of sample numbers, barcodes and Ct values
        lis_report:     Report from lab information system containing an overview of coronavirus
                        samples
    """
    # there will be a lot of external columns added - get column names now so we can cut dataframe
    # to what is needed later
    result_columns = list(new_results.columns.values)

    # filter new results
    # extract sample numbers and barcode (for identifying correct sample in case of duplicates)
    positive_control_pattern = '|'.join(workflow_config['sample_number_settings']['positive_control'].keys())
    negative_controls = workflow_config['sample_number_settings']['negative_control']
    # TODO: fix pattern?
    sample_id_pattern = f"({workflow_config['sample_number_settings']['sample_number_format']}" \
                        f"|{negative_controls}" \
                        f"|(?:{positive_control_pattern}))"
    barcode = workflow_config["barcode_format"]
    # workaround: first hit is the entire sample number capturing group, last is barcode
    new_results[["sample_number",
                 "barcode"]] = new_results["taxon"].str.extract(f"{sample_id_pattern}_({barcode})",
                                                                expand=True).iloc[:, [0,-1]]

    # remove controls
    new_results = new_results[~(new_results["sample_number"].str.fullmatch(negative_controls)
                                | new_results["sample_number"].str.fullmatch(positive_control_pattern))]


    # if we have sequenced a sample twice in this run we only want it to appear once regardless
    # remove here to avoid duplications later; keep the sample with the lowest total missing
    new_results = new_results.sort_values(by=["totalMissing"]).drop_duplicates(subset=["sample_number"],
                                                                               keep="first")
    # translate sample numbers for use with LIS
    prefix_translation = get_number_letter_combination(workflow_config["sample_number_settings"]["number_to_letter"],
                                                       workflow_config["sample_number_settings"]["sample_numbers_in"],
                                                       workflow_config["sample_number_settings"]["sample_numbers_out"])
    start_options = "|".join(prefix_translation.keys())
    start_pattern = f"^({start_options})"
    # translate prefix if found in translation, else keep it - TODO: move to helpers?
    new_results["sample_number_translated"] = new_results["sample_number"].apply(lambda x:
                                                                                 re.sub(start_pattern,
                                                                                        lambda match:
                                                                                        prefix_translation.get(
                                                                                        match.group(),
                                                                                        match.group()),
                                                                                   x))
    new_results = pd.merge(new_results, lis_report, left_on="sample_number_translated",
                           right_on="prøvenr")
    # remove research samples by CPR number
    new_results = new_results[~(new_results["cprnr."] == "0000000000")]
    # fix received date for compatibility with R (gets parsed as amount of days otherwise)
    new_results["modtaget"] = new_results["modtaget"].apply(lambda received:
                                                            date.strftime(datetime.strptime(received,
                                                                                            "%d%m%Y"),
                                                                          "%d-%m-%Y"))
    # add Ct value - make sure we get the right sample in case of duplicates
    new_results = pd.merge(new_results, samples, left_on=["sample_number", "barcode"],
                           right_on=["KMA nr", "Barkode NB"])


    # cut down to original dataframe + date received + Ct
    result_columns.extend(["modtaget", "CT/CP værdi", "sample_number"])
    new_results = new_results[result_columns]
    new_results["analyseret"] = date.strftime(date.today(), "%d-%m-%Y")
    merged_results = pd.concat([clean_results, new_results], ignore_index=True)
    # remove any duplicates that may have gotten added now - once again keep the better version
    merged_results = merged_results.sort_values(by=["totalMissing"]).drop_duplicates(subset=["sample_number"],
                                                                                     keep="first")
    # move errors to the end
    results_colnames = list(merged_results.columns.values)
    results_colnames.remove("errors")
    results_colnames.append("errors")
    return merged_results[results_colnames]


if __name__ == "__main__":
    arg_parser = ArgumentParser(description="Append individual set of results to 'raw' all_results"
                                            "and cleaned results to 'cleaned' all_results "
                                            "(removing research and failed samples and adding "
                                            "received and sequencing date as well as Ct value).")
    arg_parser.add_argument("new_results", help="Path to file containing new results")
    arg_parser.add_argument("all_results", help="Path to file containing all results")
    arg_parser.add_argument("cleaned_results",
                            help="Path to file containing cleaned version of all results")
    arg_parser.add_argument("samplesheet", help="Path to sample sheet")
    arg_parser.add_argument("--workflow_config_file",
                            help=f"Config file for workflow "
                                 f"(default: {pipeline_config.default_config_file})",
                            default=pipeline_config.default_config_file)
    args = arg_parser.parse_args()
    # set up configuration
    configfile = pathlib.Path(args.workflow_config_file)
    if not configfile.exists():
        raise FileNotFoundError("Config file not found.")
    with open(configfile, "r", encoding="utf-8") as config_file:
        workflow_config = yaml.safe_load(config_file)
    check_config.check_config(workflow_config)
    # handle results and result data
    all_results = pathlib.Path(args.all_results)
    results_to_add = pathlib.Path(args.new_results)
    all_results_data = pd.read_csv(all_results, sep=";", decimal=",", index_col=False)
    current_results_data = pd.read_csv(results_to_add, sep=";", decimal=",", index_col=False)
    cleaned_results = pathlib.Path(args.cleaned_results)
    cleaned_data = pd.read_csv(cleaned_results, sep=";", decimal=",", index_col=False,
                               dtype={"sample_number": str})

    # import and read LIS report
    lab_report = pathlib.Path(workflow_config["lab_info_system"]["lis_report"])
    lab_info_data = pd.read_csv(lab_report, encoding="latin1", dtype={"afsendt": str,
                                                                      "cprnr.": str,
                                                                      "modtaget": str})

    # get sample numbers and information from sample sheet
    sample_sheet = pathlib.Path(args.samplesheet)
    sample_data = pd.read_csv(sample_sheet, dtype={"KMA nr": str})

    # get "raw" data
    combined_results = combine_results(current_results_data, all_results_data)
    combined_results.to_csv(path_or_buf=all_results, sep=";", decimal=",", index=False)

    # get "cleaned" data
    new_cleaned = clean_and_combine(current_results_data, cleaned_data, sample_data, lab_info_data)
    new_cleaned.to_csv(path_or_buf=cleaned_results, sep=";", decimal=",", index=False)
